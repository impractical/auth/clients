module impractical.co/auth/clients

replace impractical.co/auth/hmac v0.0.0 => ../hmac

require (
	bitbucket.org/ww/goautoneg v0.0.0-20120707110453-75cd24fc2f2c // indirect
	darlinggo.co/api v0.0.0-20160924005218-06eb95038fc2
	darlinggo.co/pan v0.2.0
	darlinggo.co/trout v1.0.1
	github.com/go-sql-driver/mysql v1.4.1 // indirect
	github.com/gobuffalo/packr v1.22.0 // indirect
	github.com/hashicorp/go-immutable-radix v1.0.0 // indirect
	github.com/hashicorp/go-memdb v0.0.0-20180223233045-1289e7fffe71
	github.com/hashicorp/go-uuid v1.0.0
	github.com/lib/pq v1.0.0
	github.com/mattn/go-sqlite3 v1.10.0 // indirect
	github.com/pborman/uuid v1.2.0 // indirect
	github.com/rubenv/sql-migrate v0.0.0-20180704111356-3f452fc0ebeb
	github.com/ziutek/mymysql v1.5.4 // indirect
	google.golang.org/appengine v1.4.0 // indirect
	gopkg.in/gorp.v1 v1.7.2 // indirect
	impractical.co/auth/hmac v0.0.0
	impractical.co/userip v0.0.0-20190512094304-7102ae861013
	yall.in v0.0.1
)
